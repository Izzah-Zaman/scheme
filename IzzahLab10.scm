
(define sub(lambda (m n) n pred m))

(define and (lambda (m n) (lambda (a b) (m (n a b) b))))

(define or (lambda (m n) (lambda (a b) (m a (n a b)))))

(define not(lambda (m) (lambda (f g) (m g f))))

(define leq(lambda (m n) (iszero ( sub m n) )))

(define geq(lambda (m n) (iszero ( sub n m) )))